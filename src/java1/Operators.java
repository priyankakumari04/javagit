package java1;

public class Operators {
	public static void main(String[] args) {
		int i = 5;
		int j = 7;
		int k = 9;
		int sum = i++ + ++j + j-- + k++ + --k + k;
		System.out.println(i);
		System.out.println(j);
		System.out.println(k);
		System.out.println(sum);
	}

}
